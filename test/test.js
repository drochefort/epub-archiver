const fs = require('fs');
const shell = require('shelljs');
const chai = require('chai');
const chaiFiles = require('chai-files');
const { zipEpub, unzipEpub } = require('../index');

// setup chai assertions
const {expect} = chai;
const {file} = chaiFiles;
chai.use(chaiFiles);

// constants
const temp = 'test/temp';
const epubFileName = `${temp}/test.epub`;

describe('zip/unzip epubs', () => {
    before(done => {
        shell.rm('-rf', temp);
        fs.mkdir(temp, done);
    });

    it('should zip files into an epub archive', () => {
        return zipEpub('app', epubFileName)
            .then(filename => expect(filename).to.equal(epubFileName));
    });

    it('should unzip files from an epub archive', () => {
        return unzipEpub(epubFileName, temp)
            .then(outputPath => {
                expect(file('lib/archive.js')).to.exist;
                expect(outputPath).to.equal(temp);
            });
    });
});
