const { zipEpub, unzipEpub } = require('./lib/archive');

module.exports = { zipEpub, unzipEpub };