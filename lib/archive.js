/**
 * EPUB ZIP/UNZIP FEATURE
 */
const fs = require('fs');
const shell = require('shelljs');
const glob = require('glob');
const yazl = require('yazl');
const yauzl = require('yauzl');


/**
 * Zip directory content to EPUB
 * 
 * @param directory {string} directory where to take the files from 
 * @param epubFileName {string} name of the target EPUB file
 */
function zipEpub(directory, epubFileName) {

    function rebase(path) {
        return path.replace(`${directory}/`, '');
    }

    return new Promise((resolve, reject) => {
        const zipFile = new yazl.ZipFile();

        glob(`${directory}/**`, { dot: true }, (err, files) => {
            if (err) reject(err);

            // Add mimetype first
            files.filter(file => file.includes('mimetype'))
                .forEach(file => {
                    zipFile.addFile(file, rebase(file), {
                        compress: false
                    });
                });

            // Then add other files
            files.forEach(file => {
                if (shell.test('-d', file)) {
                    zipFile.addEmptyDirectory(rebase(file));
                } else if (!file.includes('mimetype')) {
                    zipFile.addFile(file, rebase(file), {
                        compress: !files.includes('META-INF/')
                    });
                }
            });

            const epubFile = fs.createWriteStream(epubFileName);
            zipFile.outputStream.pipe(epubFile)
                .on('close', () => {
                    console.log(epubFileName, 'zipped.');
                    resolve(epubFileName);
                })
                .on('error', e => {
                    console.error(epubFileName, 'zip error.');
                    reject(e);
                });

            zipFile.end();
        });
    });
}

/**
 * Unzip an EPUB file into the directory specified by outputPath
 * 
 * @param epubFileName {string} name of the EPUB file to unzip
 * @param outputPath {string} name of the target path for the unzipped files 
 */
function unzipEpub(epubFileName, outputPath) {
    return new Promise((resolve, reject) => {
        if (!shell.test('-e', epubFileName)) {
            reject('file does not exist', epubFileName);
        }

        yauzl.open(epubFileName, function (err, zipfile) {
            if (err) reject(err);
            zipfile.on('close', function () {
                console.log(epubFileName, 'unzipped.');
                resolve(outputPath);
            });

            zipfile.on('entry', function (entry) {
                if (/\/$/.test(entry.fileName)) {
                    // skip directories : directory file names end with '/'
                    return;
                }
                zipfile.openReadStream(entry, function (err, readStream) {
                    if (err) reject(err);
                    var outFileName = [outputPath, entry.fileName].join('/');
                    var outSubFolderParts = outFileName.split('/');
                    outSubFolderParts.pop();

                    shell.mkdir('-p', outSubFolderParts.join('/')); // ensurePathExists
                    readStream.pipe(fs.createWriteStream(outFileName));
                });
            });
        });
    });
}

module.exports = { zipEpub, unzipEpub };