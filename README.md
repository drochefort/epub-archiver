# EPUB Archiver #

This package provides the necessary methods to zip and unzip and EPUB file.

## Install ##

```shell
npm install @hmh/epub-archiver --save
```

## Usage ##

**Important note**: all APIs are asynchronous and return a `Promise`.

### Compress the content of a directory to an EPUB

```javascript
const { zipEpub } = require('@hmh/epub-archiver');

zipEpub(directory, epubFileName)
    .then(filename => /* do something useful with EPUB */);
```

### Expand and EPUB in the specified directory

```javascript
const { zipEpub } = require('@hmh/epub-archiver');

unzipEpub(epubFileName, directory)
    .then(filename => /* do something useful with EPUB */);
```

## Tests ##

```shell
npm install mocha -g
npm install
npm test
```

### Contribution guidelines ###

* All contributions via pull requests
* At least one unit test for each pull request

### Who do I talk to? ###

* @drochefort